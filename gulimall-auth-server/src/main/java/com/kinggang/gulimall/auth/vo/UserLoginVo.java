package com.kinggang.gulimall.auth.vo;

import lombok.Data;

@Data
public class UserLoginVo {

    private String loginAcct;
    private String password;
}
