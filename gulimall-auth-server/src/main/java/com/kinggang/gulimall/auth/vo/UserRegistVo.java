package com.kinggang.gulimall.auth.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class UserRegistVo {

    @NotEmpty(message = "用户名必须不为空")
    @Length(min = 6, max = 18, message = "用户名长度要为6-18位")
    private String username;

    @NotEmpty(message = "密码必须不为空")
    @Length(min = 6, max = 18, message = "密码长度要为6-18位")
    private String password;

    @NotEmpty(message = "手机号必须不为空")
//    @Pattern(regexp = "^[1]([3-9])[0-9]{0}$", message = "手机号格式不正确")
    private String phone;

    @NotEmpty(message = "验证码必须不为空")
    private String code;
}
