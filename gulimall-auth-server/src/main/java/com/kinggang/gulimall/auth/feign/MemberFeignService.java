package com.kinggang.gulimall.auth.feign;

import com.kinggang.common.utils.R;
import com.kinggang.gulimall.auth.vo.SocialUserVo;
import com.kinggang.gulimall.auth.vo.UserLoginVo;
import com.kinggang.gulimall.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "gulimall-member")
public interface MemberFeignService {

    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo registVo);

    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("/member/member/oauth2/login")
    R oauthLogin(@RequestBody SocialUserVo socialUserVo)throws Exception;
}
