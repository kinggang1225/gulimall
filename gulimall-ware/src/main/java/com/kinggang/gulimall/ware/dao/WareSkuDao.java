package com.kinggang.gulimall.ware.dao;

import com.kinggang.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:48:02
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    void addStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);

    Long getSkusHasStock(@Param("item") Long item);

    List<Long> listWareHasStock(@Param("skuId") Long skuId);

    Long lockSkuStock(@Param("wareId") Long wareId, @Param("skuId") Long skuId, @Param("lockNum") Integer lockNum);


    void unLockStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("num") Integer num);
}
