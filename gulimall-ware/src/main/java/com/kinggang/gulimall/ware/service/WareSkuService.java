package com.kinggang.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.to.SkuHasStockVo;
import com.kinggang.common.to.mq.OrderTo;
import com.kinggang.common.to.mq.StockLockedTo;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.ware.entity.WareSkuEntity;
import com.kinggang.gulimall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:48:02
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockVo> getSkusHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);

    void buildUnlockStock(StockLockedTo stockLockedTo);

    void buildUnlockStock(OrderTo orderTo);
}

