package com.kinggang.gulimall.ware.dao;

import com.kinggang.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:48:03
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
