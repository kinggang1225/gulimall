package com.kinggang.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.ware.dao.WareInfoDao;
import com.kinggang.gulimall.ware.entity.WareInfoEntity;
import com.kinggang.gulimall.ware.feign.MemberFeignService;
import com.kinggang.gulimall.ware.service.WareInfoService;
import com.kinggang.gulimall.ware.vo.FareVo;
import com.kinggang.gulimall.ware.vo.MemberAddressVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Map;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Autowired
    MemberFeignService memberFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            wrapper.eq("id", key)
                    .or().like("name", key).or().like("address", key).or().like("areacode", key);
        }
        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    @Override
    public FareVo getFare(Long addrId) {
        FareVo fareVo = new FareVo();
        //远程调用会员服务查收货地址信息
        R r = memberFeignService.addrInfo(addrId);
        MemberAddressVo data = r.getData("memberReceiveAddress", new TypeReference<MemberAddressVo>() {
        });
        if (data != null) {
            //截取手机号最后一位作为运费
            String phone = data.getPhone();
            String Fare = phone.substring(phone.length() - 1, phone.length());
            BigDecimal fare = new BigDecimal(Fare);
            fareVo.setFare(fare);
            fareVo.setAddress(data);
            return fareVo;
        }
        return null;
    }

}