package com.kinggang.gulimall.ware.dao;

import com.kinggang.gulimall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:48:03
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
