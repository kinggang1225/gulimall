package com.kinggang.gulimall.ware.exception;

public class NoStockException extends RuntimeException {

//    private Long skuId;

    public NoStockException(Long skuId) {
        super("商品id:"+skuId + "没有库存了");
    }
}
