package com.kinggang.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.to.SkuHasStockVo;
import com.kinggang.common.to.mq.OrderTo;
import com.kinggang.common.to.mq.StockDetailTo;
import com.kinggang.common.to.mq.StockLockedTo;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.ware.dao.WareSkuDao;
import com.kinggang.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.kinggang.gulimall.ware.entity.WareOrderTaskEntity;
import com.kinggang.gulimall.ware.entity.WareSkuEntity;
import com.kinggang.gulimall.ware.exception.NoStockException;
import com.kinggang.gulimall.ware.feign.OrderFeignService;
import com.kinggang.gulimall.ware.feign.ProductFeignService;
import com.kinggang.gulimall.ware.service.WareOrderTaskDetailService;
import com.kinggang.gulimall.ware.service.WareOrderTaskService;
import com.kinggang.gulimall.ware.service.WareSkuService;
import com.kinggang.gulimall.ware.vo.OrderItemVo;
import com.kinggang.gulimall.ware.vo.OrderVo;
import com.kinggang.gulimall.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    ProductFeignService feignService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    WareOrderTaskService wareOrderTaskService;

    @Autowired
    WareOrderTaskDetailService wareOrderTaskDetailService;

    @Autowired
    OrderFeignService orderFeignService;

    /**
     * 解锁库存
     */
    public void buildUnlockStock(StockLockedTo to) {
        StockDetailTo stockDetail = to.getStockDetail();
        Long detailId = stockDetail.getId();
        WareOrderTaskDetailEntity detailEntity = wareOrderTaskDetailService.getById(detailId);
        if (detailEntity != null) {
            //有库已经生成
            Long id = to.getId();
            WareOrderTaskEntity taskEntity = wareOrderTaskService.getById(id);
            String orderSn = taskEntity.getOrderSn();
            //根据订单号去订单服务查询订单创建情况
            R r = orderFeignService.getOrderStatus(orderSn);
            if (r.getCode() == 0) {
                OrderVo orderVo = r.getData(new TypeReference<OrderVo>() {
                });
                if (orderVo == null || orderVo.getStatus() == 4) {
                    //订单已经取消了 或者订单直接不存在 可以解锁库存
                    if (detailEntity.getLockStatus() == 1) {
                        //状态为已锁定时才能解锁操作
                        unLockStock(stockDetail.getSkuId(), stockDetail.getWareId(), stockDetail.getSkuNum(), stockDetail.getId());
                    }
                }
            } else {
                //远程调用异常
                throw new RuntimeException("远程调用异常");
            }
        } else {
            //无需解锁
        }
    }

    //订单过期后解锁库存
    @Transactional
    @Override
    public void buildUnlockStock(OrderTo orderTo) {
        String orderSn = orderTo.getOrderSn();
        //根据订单号查询库存工作单
        WareOrderTaskEntity orderTaskEntity = wareOrderTaskService.getOne(new QueryWrapper<WareOrderTaskEntity>().eq("order_sn", orderSn));
        Long taskEntityId = orderTaskEntity.getId();
        //查询所有未解锁的库存
        List<WareOrderTaskDetailEntity> detailEntityList = wareOrderTaskDetailService.list(new QueryWrapper<WareOrderTaskDetailEntity>().eq("task_id", taskEntityId).eq("lock_status", 1));
        //解锁库存
        System.out.println("收到订单关闭，准备解锁库存的信息...");
        for (WareOrderTaskDetailEntity entity : detailEntityList) {
            this.unLockStock(entity.getSkuId(), entity.getWareId(), entity.getSkuNum(), entity.getId());
        }
    }

    //解锁库存
    private void unLockStock(Long skuId, Long wareId, Integer num, Long detailId) {
        System.out.println("订单已经取消了或者订单异常不存在开始解锁库存........");
        baseMapper.unLockStock(skuId, wareId, num);
        //更新工作单状态为已解锁
        WareOrderTaskDetailEntity detailEntity = new WareOrderTaskDetailEntity();
        detailEntity.setId(detailId);
        detailEntity.setLockStatus(2);
        wareOrderTaskDetailService.updateById(detailEntity);
    }


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> wrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if (!StringUtils.isEmpty(skuId)) {
            wrapper.eq("sku_id", skuId);
        }
        String wareId = (String) params.get("wareId");
        if (!StringUtils.isEmpty(wareId)) {
            wrapper.eq("ware_id", wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    //入库
    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        //判断是新增还是增加库存
        List<WareSkuEntity> wareSkuEntities = this.list(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));
        if (wareSkuEntities == null || wareSkuEntities.size() == 0) {
            //新增库存
            WareSkuEntity skuEntity = new WareSkuEntity();
            skuEntity.setSkuId(skuId);
            skuEntity.setStock(skuNum);
            skuEntity.setWareId(wareId);
            skuEntity.setStockLocked(0);
            try {
                //TODO 远程查询sku的名字 如果失败无需要回滚
                R info = feignService.info(skuId);
                Map<String, Object> data = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0) {
                    skuEntity.setSkuName((String) data.get("skuName"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.save(skuEntity);
        } else {
            this.baseMapper.addStock(skuId, wareId, skuNum);
        }
    }

    @Override
    public List<com.kinggang.common.to.SkuHasStockVo> getSkusHasStock(List<Long> skuIds) {
        List<SkuHasStockVo> collect = skuIds.stream().map(item -> {
            SkuHasStockVo vo = new SkuHasStockVo();
            Long hasStock = baseMapper.getSkusHasStock(item);
            vo.setSkuId(item);
            vo.setHasStock(hasStock == null ? false : true);
            return vo;
        }).collect(Collectors.toList());
        return collect;
    }

    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {

        //操作库存工作单
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        wareOrderTaskService.save(taskEntity);

        List<OrderItemVo> voLocks = vo.getLocks();
        List<SkuWareHasStock> hasStocks = voLocks.stream().map(item -> {
            SkuWareHasStock wareHasStock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            wareHasStock.setSkuId(skuId);
            wareHasStock.setLockNum(item.getCount());
            List<Long> wareIds = baseMapper.listWareHasStock(skuId);
            wareHasStock.setWareIds(wareIds);
            return wareHasStock;
        }).collect(Collectors.toList());

        for (SkuWareHasStock hasStock : hasStocks) {
            Boolean skuStocked = false;
            Long skuId = hasStock.getSkuId();
            List<Long> wareIds = hasStock.getWareIds();
            if (wareIds == null || wareIds.size() == 0) {
                //都没有库存
                throw new NoStockException(skuId);
            }
            for (Long wareId : wareIds) {
                Long count = baseMapper.lockSkuStock(wareId, skuId, hasStock.getLockNum());
                if (count == 1) {
                    //锁定成功
                    skuStocked = true;
                    WareOrderTaskDetailEntity orderTaskDetailEntity = new WareOrderTaskDetailEntity(null, skuId, null, hasStock.getLockNum(), taskEntity.getId(), wareId, 1);
                    wareOrderTaskDetailService.save(orderTaskDetailEntity);

                    //告诉MQ锁库存成功 并把库存记录的id发送给mq
                    StockDetailTo detailTo = new StockDetailTo();
                    BeanUtils.copyProperties(orderTaskDetailEntity, detailTo);
                    StockLockedTo lockedTo = new StockLockedTo();
                    lockedTo.setId(taskEntity.getId());
                    lockedTo.setStockDetail(detailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", lockedTo);
                    break;
                }
            }
            if (skuStocked == false) {
                //当前商品锁定失败
                throw new NoStockException(skuId);
            }
        }
        //到这一步肯定锁库存成功
        return true;
    }

    @Data
    class SkuWareHasStock {
        private Long skuId;
        private Integer lockNum;
        private List<Long> wareIds;
    }

}