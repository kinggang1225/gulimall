package com.kinggang.gulimall.ware.vo;

import lombok.Data;

//采购项vo
@Data
public class PurchaseItemDoneVo {
    private Long itemId;
    private Integer status;
    private String reason;
}
