package com.kinggang.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.ware.entity.PurchaseEntity;
import com.kinggang.gulimall.ware.vo.MergeVo;
import com.kinggang.gulimall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:48:03
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceive(Map<String, Object> params);

    void mergePurchase(MergeVo vo);

    void received(List<Long> ids);

    void finish(PurchaseDoneVo doneVo);
}

