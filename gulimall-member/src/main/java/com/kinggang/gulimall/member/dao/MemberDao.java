package com.kinggang.gulimall.member.dao;

import com.kinggang.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 13:03:54
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
