package com.kinggang.gulimall.member.dao;

import com.kinggang.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 13:03:53
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {

    public MemberLevelEntity getDefaultLevel();
	
}
