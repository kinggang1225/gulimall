package com.kinggang.gulimall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.utils.HttpUtils;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.gulimall.member.dao.MemberDao;
import com.kinggang.gulimall.member.dao.MemberLevelDao;
import com.kinggang.gulimall.member.entity.MemberEntity;
import com.kinggang.gulimall.member.entity.MemberLevelEntity;
import com.kinggang.gulimall.member.exception.PhoneExistException;
import com.kinggang.gulimall.member.exception.UsernameExistException;
import com.kinggang.gulimall.member.service.MemberService;
import com.kinggang.gulimall.member.vo.MemberLoginVo;
import com.kinggang.gulimall.member.vo.MemberRegistVo;
import com.kinggang.gulimall.member.vo.SocialUserVo;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberRegistVo registVo) {
        MemberEntity entity = new MemberEntity();
        //查询默认等级
        MemberLevelEntity memberLevelEntity = memberLevelDao.getDefaultLevel();
        entity.setLevelId(memberLevelEntity.getId());

        //用户名和手机号要唯一
        checkPhoneUnique(registVo.getPhone());
        checkUsernameUnique(registVo.getUsername());

        entity.setUsername(registVo.getUsername());
        entity.setMobile(registVo.getPhone());
        entity.setNickname(registVo.getUsername());

        //密码要加密保存
        String encode = new BCryptPasswordEncoder().encode(registVo.getPassword());
        entity.setPassword(encode);

        //保存数据库
        save(entity);
    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneExistException {
        int count = count(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (count > 0) {
            throw new PhoneExistException();
        }
    }

    @Override
    public void checkUsernameUnique(String username) throws UsernameExistException {
        int count = count(new QueryWrapper<MemberEntity>().eq("username", username));
        if (count > 0) {
            throw new UsernameExistException();
        }
    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {
        String loginAcct = vo.getLoginAcct();
        String password = vo.getPassword();
        //查询数据库
        MemberEntity entity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("username", loginAcct)
                .or().eq("mobile", loginAcct));
        if (entity == null) {
            return null;
        } else {
            String passwordDb = entity.getPassword();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            boolean matches = passwordEncoder.matches(password, passwordDb);
            if (matches) {
                return entity;
            } else {
                return null;
            }
        }
    }

    @Override
    public MemberEntity oauthLogin(SocialUserVo vo) throws Exception {
        //先判断用户是否已经登陆过
        MemberEntity entity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", vo.getUid()));
        if (entity != null) {
            //以前登陆过 那么就更新其数据
            MemberEntity entity1 = new MemberEntity();
            entity1.setExpiresIn(vo.getExpires_in());
            entity1.setAccessToken(vo.getAccess_token());
            entity1.setId(entity.getId());
            updateById(entity1);
            //已经社交登陆过了 返回用户信息
            entity.setAccessToken(vo.getAccess_token());
            entity.setExpiresIn(vo.getExpires_in());
            return entity;
        } else {
            //用户第一次登录 就进行注册
            MemberEntity regist = new MemberEntity();

            try {
                //查寻当前社交账户的信息
                Map<String, String> query = new HashMap<>();
                query.put("access_token", vo.getAccess_token());
                query.put("uid", vo.getUid());
                HttpResponse response = HttpUtils.doPost("https://api.weibo.com", "/2/users/show.json", "post", new HashMap<String, String>(), null,query);
                if (response.getStatusLine().getStatusCode() == 200) {
                    //查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    //将json数据转成json对象
                    JSONObject jsonObject = JSON.parseObject(json);
                    //或其渠道昵称
                    String nickName = jsonObject.getString("name");
                    //获取到性别
                    String gender = jsonObject.getString("gender");
                    //设置到要注册的对象中
                    regist.setNickname(nickName);
                    regist.setGender("m".equals(gender)?1:0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //防止网络问题 出现问题  首要设置关键信息
            regist.setSocialUid(vo.getUid());
            regist.setAccessToken(vo.getAccess_token());
            regist.setExpiresIn(vo.getExpires_in());

            //保存注册信息
            save(regist);
            return regist;
        }
    }

}