package com.kinggang.gulimall.member.controller;

import com.kinggang.common.exception.BizCodeEnume;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.member.entity.MemberEntity;
import com.kinggang.gulimall.member.exception.PhoneExistException;
import com.kinggang.gulimall.member.exception.UsernameExistException;
import com.kinggang.gulimall.member.service.MemberService;
import com.kinggang.gulimall.member.vo.MemberLoginVo;
import com.kinggang.gulimall.member.vo.MemberRegistVo;
import com.kinggang.gulimall.member.vo.SocialUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 会员
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 13:03:54
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    //注册
    @PostMapping("/regist")
    public R regist(@RequestBody MemberRegistVo registVo) {
        try {
            memberService.regist(registVo);
        } catch (UsernameExistException e) {
            //处理检查手机号 用户名是否唯一的异常
            return R.error(BizCodeEnume.USER_EXIST_EXCEPTION.getCode(), BizCodeEnume.USER_EXIST_EXCEPTION.getMsg());
        } catch (PhoneExistException e) {
            //处理检查手机号 用户名是否唯一的异常
            return R.error(BizCodeEnume.PHONE_EXIST_EXCEPTION.getCode(), BizCodeEnume.PHONE_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    //社交登录
    @PostMapping("/oauth2/login")
    public   R oauthLogin(@RequestBody SocialUserVo vo) throws Exception {
    MemberEntity entity = memberService.oauthLogin(vo);
        if (entity != null) {
            return R.ok().setData(entity);
        } else {
            return R.error(BizCodeEnume.USERNAME_PASSWORD_VAILD_EXCEPTION.getCode(), BizCodeEnume.USERNAME_PASSWORD_VAILD_EXCEPTION.getMsg());
        }
    }

    //登录
    @PostMapping("/login")
    public R login(@RequestBody MemberLoginVo vo) {
        MemberEntity entity = memberService.login(vo);
        if (entity != null) {
            return R.ok().setData(entity);
        } else {
            return R.error(BizCodeEnume.USERNAME_PASSWORD_VAILD_EXCEPTION.getCode(), BizCodeEnume.USERNAME_PASSWORD_VAILD_EXCEPTION.getMsg());
        }
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id) {
        MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member) {
        memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member) {
        memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids) {
        memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
