package com.kinggang.gulimall.member.vo;


import lombok.Data;

@Data
public class MemberLoginVo {
    private String loginAcct;
    private String password;
}
