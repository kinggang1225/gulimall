package com.kinggang.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.member.entity.MemberEntity;
import com.kinggang.gulimall.member.exception.PhoneExistException;
import com.kinggang.gulimall.member.exception.UsernameExistException;
import com.kinggang.gulimall.member.vo.MemberLoginVo;
import com.kinggang.gulimall.member.vo.MemberRegistVo;
import com.kinggang.gulimall.member.vo.SocialUserVo;

import java.util.Map;

/**
 * 会员
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 13:03:54
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo registVo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUsernameUnique(String username) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity oauthLogin(SocialUserVo vo) throws Exception;
}

