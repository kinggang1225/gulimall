package com.kinggang.common.to;

import lombok.Data;

import java.math.BigDecimal;

//远程数据调用的实体类
@Data
public class SpuBoundTo {

    private Long spuId;

    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
