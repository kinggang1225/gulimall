package com.kinggang.common.exception;

public class NoStockException extends RuntimeException {

    public NoStockException(String msg) {
        super(msg);
    }

    public NoStockException(){
        super("没有库存了");
    }

//    private Long skuId;

    public NoStockException(Long skuId) {
        super("商品id:"+skuId + "没有库存了");
    }
}
