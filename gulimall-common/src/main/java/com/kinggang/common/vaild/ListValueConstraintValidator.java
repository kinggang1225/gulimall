package com.kinggang.common.vaild;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

//ListValue校验器
public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {
    Set<Integer> sets = new HashSet<>();
    //初始化方法
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int[] values= constraintAnnotation.value();
        for (int value : values) {
            sets.add(value);
        }
    }

    //校验成功
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return sets.contains(value);
    }
}
