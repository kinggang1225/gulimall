package com.kinggang.gulimall.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    public void createExchange() {
        //创建一个交换机
        DirectExchange exchange = new DirectExchange("hello-java-exchange", true, false);
        //添加交换机
        amqpAdmin.declareExchange(exchange);
    }

    @Test
    public void createQueue() {
        amqpAdmin.declareQueue(new Queue("hello-java-queue", true, false, false));
    }

    @Test
    public void createBinding() {
        amqpAdmin.declareBinding(new Binding("hello-java-queue", Binding.DestinationType.QUEUE,
                "hello-java-exchange", "hello.java", null));
    }

    @Test
    public void sendMessage() {
        rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", "Hello World");
    }


}
