package com.kinggang.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.kinggang.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private String app_id = "2021000116686847";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCbFU6+vQF6TXBz5PEJFTk49w4VVK356fO5m92DveBDNNooTWBY1XMWwuvI6hU64o0/B0jXKVycHLAZ5trH9jSornCODGgzLepA5nGjoCITnTSq2w/n8GyJwueBUbHn3TaCAK6TZnNRHbmfOfDZ88DMMLzkEo1JXJqnAbXpRzPsIscE+Gxj8VRgnZYQBudv+UaPk7IV4dItb8vIwZQlCpVUs1Jvt0XrunvbkeZ0OeJoNNobq/LvNHCxyFVgbu0j+/ZceTiB6NqrC8Y6+ao3vkpdbZ6OK4juPad+Iac3BSYzmAnhtwFpEVmT4hZeBJbQIoIhy4S+iC/MFjBc0FvfyhRDAgMBAAECggEBAJMT8CyBAAfHraCcPeSJVk2qMJcWSjE8KcOcCMdKqofSdhvG1lsphA7QKeHP7vZ7qc1e0TwNpCECLBlEkbUW0PYxCdoN9jyjjQikBJ5T3j2P2EU5oKotoCI+0GQa4IiviWRd1TNB9Je3gzV5M+zgebatelcrKQesZZeBzc5BkwKcszqGNGT0KFeyZ+AQzGZvp9+DnkHjWAm8reNJipnupqQ1n9CA9QBJ1zeO7XcMp1DSE3f/Sv1KNJNP4t89xakmXQ1g0/Q84vLHdxVtnwv4VDpXdD5477sUfgaKCOBC572flOB517vfzEs43gBDqgexNRyVUmEvc2mdRyEQWb5xM8ECgYEA5ZId7FXfmtDKhCdO3HFb6dlu6xnH8I1+Ml6FZj0I5Id8NuT5kfDwzMPTcBiO/Yd6Yz3AiSI6CvbUJw+o7CMv3QcGe7cjuKLYzGQttJhZwD8CdnUgqbrBtjzN4xpMU2Z5Mpp4ogwyFwQF7gojirQYEsVmiLMKNa+/svgfltPJCbECgYEArO/nFV+Li6EpXFFAVyVp4DhTGSgqah4O+eK8sdNO/4h4q6qiRAAyS5n4v1XJER/KLBgGRaS5aVcg72AmgPQ6ARVV3g8yRJ+Dkr3kO9VaAtR0VtJiOPuQ/j/3vwdDj6eEw0K8FfEzSGoo8YSp3uEnPRRWn1hVSfyBQaLnLP5zBjMCgYEAx/0WXEAi3zdsdNvZc2i9bWahneWDrCdW/IIjLAGXsZ3ls9ydHWoMHBWsKEb8qhhPogy1ocFTVyHXh3+45J1yoNXx6+yLDdmvi77poh3mcfdyKce3gKTVA/DMWz8Zpr3qbw7qcCC3sh0rHUdv1vE014Vn4UY3arWEBmDpA/fTdsECgYEAj9Y1oGxIl4ki6prV5eXDau/OBfWHWZ3InQR+J3V2lkj+/OE+d0qmioT1BMcoJisHr6cHyHMal8gVJ6QUQltfxzjko36GM9dsyudP5ogshoAIh5ISigyK6z1vJR64jcoqPXHLGID2Q3uwtDjARRWF2mKJLZ19yrzPETWaC61TJEsCgYBxlc8rwP2Mm8GC4fTzrGeX0VA6ppugEohPOLjzE/SkHV9E7B8igRssj84QMEf3eupIwCO83ztGuAJ2XGAV7zqFfb2VYohrdYa3l79VBIlz1MC2VyKOJFfi2PavRgN1xz9F3DDheH1JIZ5D/B0ABsv/5EEONet3MZm7hs+X4eauQA==";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmI8gFtDJrB6AjMBG8Mc3Nx3qQac85EtS1uzhDEwvYgTASrHLDzE0jMmHf8QVtJh3lY9CMsiKZhbTu1zYflCzOlmFPeavBamy/PSprL8lP6b5r71bgv0vbDOCs3S1kIQvQjmwkF8pVrpMfxJQSC9rEmvEA/XqXYnzj2KqVSBWsBSjVIbd6VyrmaGSZ2NZw9tJ+nrBUMyJjw/rfI8wy/49So62U/ewIuL+gRGQq8QO/xWPYaxdFWW/+AHd4WB+W3w+aiK0ZadrL0tBbvQaDfqfmOdsfyr1z8PLnlOuNLudyyFe++R+gwJv2NtBuPl9xZkdiADiNXVfHFbmYw/gokM9MQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private String notify_url = "http://king.free.vipnps.vip/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private String sign_type = "RSA2";

    // 字符编码格式
    private String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    //订单超时时间
    private String timeout="30m";

    public String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应：" + result);

        return result;

    }
}
