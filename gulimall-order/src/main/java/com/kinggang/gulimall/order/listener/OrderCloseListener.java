package com.kinggang.gulimall.order.listener;


import com.kinggang.gulimall.order.entity.OrderEntity;
import com.kinggang.gulimall.order.service.OrderService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RabbitListener(queues = "order.release.order.queue")
public class OrderCloseListener {

    @Autowired
    OrderService orderService;

    @RabbitHandler
    public void orderClose(OrderEntity orderEntity, Message message, Channel channel) throws IOException {
        System.out.println("已收到订单消息..."+orderEntity.getOrderSn());
        try {
            orderService.orderClose(orderEntity);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (IOException e) {
            //出现异常 消息重回队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
            e.printStackTrace();
        }

    }


}
