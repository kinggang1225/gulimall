package com.kinggang.gulimall.order.to;

import com.kinggang.gulimall.order.entity.OrderEntity;
import com.kinggang.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
public class OrderCreateTo {

    private OrderEntity orderEntity;

    private List<OrderItemEntity> items;

    private BigDecimal payPrice; // 订单计算的应付价格

    // TODO 暂未实现
    private BigDecimal fare; // 运费

}
