package com.kinggang.gulimall.order.feign;


import com.kinggang.gulimall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@FeignClient("gulimall-cart")
@Component(value ="cartFeignService" )
public interface CartFeignService {

    @GetMapping("/currentUserCartItems")
    @ResponseBody
    List<OrderItemVo> getCurrentUserCartItems();

    @GetMapping("/hello")
    @ResponseBody
    String hello();
}
