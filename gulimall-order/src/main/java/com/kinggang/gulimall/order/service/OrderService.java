package com.kinggang.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.to.mq.SecKillOrderTo;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.order.entity.OrderEntity;
import com.kinggang.gulimall.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:54:29
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    SubmitOrderRespVo submitOrder(OrderSubmitVo vo);

    void orderClose(OrderEntity orderEntity);

    PayVo getOrderPay(String orderSn);

    PageUtils queryPageWithItem(Map<String, Object> params);

    String handlePayResult(PayAsyncVo vo);

    void createSeckillOrder(SecKillOrderTo to);
}

