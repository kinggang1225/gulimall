package com.kinggang.gulimall.order.dao;

import com.kinggang.gulimall.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:54:28
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
