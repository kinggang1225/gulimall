package com.kinggang.gulimall.order.web;

import com.alipay.api.AlipayApiException;
import com.kinggang.gulimall.order.config.AlipayTemplate;
import com.kinggang.gulimall.order.service.OrderService;
import com.kinggang.gulimall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PayWebController {

    @Autowired
    AlipayTemplate alipayTemplate;
    @Autowired
    OrderService orderService;

    @GetMapping(value = "/payOrder", produces = "text/html")
    @ResponseBody
    public String payOrder(@RequestParam("orderSn") String orderSn) throws AlipayApiException {
        PayVo payVo = orderService.getOrderPay(orderSn);
        String response = alipayTemplate.pay(payVo);
        System.out.println("哈哈哈哈" + response);
        return response;
    }


}
