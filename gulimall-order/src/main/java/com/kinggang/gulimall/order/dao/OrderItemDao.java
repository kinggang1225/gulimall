package com.kinggang.gulimall.order.dao;

import com.kinggang.gulimall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:54:29
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
