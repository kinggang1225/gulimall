package com.kinggang.gulimall.order.vo;

import com.kinggang.gulimall.order.entity.OrderEntity;
import lombok.Data;


@Data
public class SubmitOrderRespVo {

    private OrderEntity orderEntity;

    private Integer code; // 0成功 状态码


}
