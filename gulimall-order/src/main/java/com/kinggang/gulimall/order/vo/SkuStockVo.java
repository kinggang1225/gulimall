package com.kinggang.gulimall.order.vo;

import lombok.Data;

@Data
public class SkuStockVo {

    private Long skuId;
    private boolean hasStock;
}
