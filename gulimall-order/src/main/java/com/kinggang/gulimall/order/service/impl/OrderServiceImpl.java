package com.kinggang.gulimall.order.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.exception.NoStockException;
import com.kinggang.common.to.mq.OrderTo;
import com.kinggang.common.to.mq.SecKillOrderTo;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.common.utils.R;
import com.kinggang.common.vo.MemberRespVo;
import com.kinggang.gulimall.order.constant.OrderConstant;
import com.kinggang.gulimall.order.dao.OrderDao;
import com.kinggang.gulimall.order.entity.OrderEntity;
import com.kinggang.gulimall.order.entity.OrderItemEntity;
import com.kinggang.gulimall.order.entity.PaymentInfoEntity;
import com.kinggang.gulimall.order.enume.OrderStatusEnum;
import com.kinggang.gulimall.order.feign.CartFeignService;
import com.kinggang.gulimall.order.feign.MemberFeignService;
import com.kinggang.gulimall.order.feign.ProductFeignService;
import com.kinggang.gulimall.order.feign.WareFeignService;
import com.kinggang.gulimall.order.interceptor.LoginUserInterceptor;
import com.kinggang.gulimall.order.service.OrderItemService;
import com.kinggang.gulimall.order.service.OrderService;
import com.kinggang.gulimall.order.service.PaymentInfoService;
import com.kinggang.gulimall.order.to.OrderCreateTo;
import com.kinggang.gulimall.order.vo.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    //使用线程共享数据
    private ThreadLocal<OrderSubmitVo> orderSubmitVoThreadLocal = new ThreadLocal<>();


    @Autowired
    OrderItemService orderItemService;

    @Autowired
    MemberFeignService memberFeignService;

    @Autowired
    CartFeignService cartFeignService;

    @Autowired
    ThreadPoolExecutor executor;

    @Autowired
    WareFeignService wareFeignService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    PaymentInfoService paymentInfoService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }

    //单订返回页显示的内容
    @Override
    public OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException {
        MemberRespVo memberRespVo = LoginUserInterceptor.threadLocal.get();
        OrderConfirmVo confirmVo = new OrderConfirmVo();

        //防止线程不同共享的数据不同步
        RequestAttributes requestContextHolder = RequestContextHolder.getRequestAttributes();

        CompletableFuture<Void> getAddressFuture = CompletableFuture.runAsync(() -> {
            //远程查询会员地址信息
            RequestContextHolder.setRequestAttributes(requestContextHolder);
            List<MemberAddressVo> addressVos = memberFeignService.getAddress(memberRespVo.getId());
            confirmVo.setAddress(addressVos);
        }, executor);

        CompletableFuture<Void> getCartFuture = CompletableFuture.runAsync(() -> {
            RequestContextHolder.setRequestAttributes(requestContextHolder); // 解决异步任务拿不到ThreadLocal里的数据
            // 2、远程查询购物车所有选中的购物项
            List<OrderItemVo> orderItems = cartFeignService.getCurrentUserCartItems();
            confirmVo.setItems(orderItems);
        }, executor).thenRunAsync(() -> {
            //远程查询有没有库存
            List<OrderItemVo> items = confirmVo.getItems();
            //收集每个商品的skuId
            List<Long> skuIds = items.stream().map((item) -> {
                return item.getSkuId();
            }).collect(Collectors.toList());
            R r = wareFeignService.hasStock(skuIds);
            List<SkuStockVo> skuStockVos = r.getData(new TypeReference<List<SkuStockVo>>() {
            });
            if (skuStockVos != null) {
                //将封装了skuId 和是否有库存的信息封装到订单的库存map中
                Map<Long, Boolean> map = skuStockVos.stream().collect(Collectors.toMap(SkuStockVo::getSkuId, SkuStockVo::isHasStock));
                confirmVo.setStocks(map);
            }
        }, executor);

        //查询优惠积分信息 TODO 默认优惠信息为积分 优惠券忽略
//        Integer integration = memberRespVo.getIntegration();
//        confirmVo.setIntegration(integration);

        //总价是直接计算出来的
//        BigDecimal totalPrice = confirmVo.getTotal();

        //TODO 防重复 提交令牌
        String token = UUID.randomUUID().toString().replace("-", "");
        confirmVo.setOrderToken(token);
        redisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRespVo.getId(), token, 30, TimeUnit.MINUTES);

        CompletableFuture.allOf(getAddressFuture, getCartFuture).get();
        return confirmVo;
    }

    //提交订单
    //@GlobalTransactional//使用分布式事务
    @Override
    @Transactional
    public SubmitOrderRespVo submitOrder(OrderSubmitVo vo) {
        orderSubmitVoThreadLocal.set(vo);
        SubmitOrderRespVo respVo = new SubmitOrderRespVo();
        MemberRespVo memberRespVo = LoginUserInterceptor.threadLocal.get();
        String token = vo.getOrderToken();
        respVo.setCode(0);

        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        Long result = redisTemplate.execute(new DefaultRedisScript<>(script, Long.class),
                Arrays.asList(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRespVo.getId()), token);
        if (result == 0L) {
            respVo.setCode(1);
            return respVo;
        } else {
            //删除成功 验证通过 完成下单操作
            OrderCreateTo order = createOrder();
            //验价 误差在0.01以内都算验价成功
            BigDecimal payAmount = order.getOrderEntity().getPayAmount();
            BigDecimal payPrice = vo.getPayPrice();
            if (Math.abs(payAmount.subtract(payPrice).doubleValue()) < 0.01) {
                //保存订单到数据库
                saveOrder(order);
                //锁库存 如果锁库存失败就要撤掉订单
                WareSkuLockVo lockVo = new WareSkuLockVo();
                lockVo.setOrderSn(order.getOrderEntity().getOrderSn());
                List<OrderItemVo> locks = order.getItems().stream().map(item -> {
                    OrderItemVo orderItemVo = new OrderItemVo();
                    orderItemVo.setSkuId(item.getSkuId());
                    orderItemVo.setCount(item.getSkuQuantity());
                    orderItemVo.setTitle(item.getSkuName());
                    return orderItemVo;
                }).collect(Collectors.toList());
                lockVo.setLocks(locks);
                //远程锁库存
                R r = wareFeignService.orderLockStock(lockVo);
                if (r.getCode() == 0) {
                    //锁库存成功
                    respVo.setOrderEntity(order.getOrderEntity());
//                    int i=10/0;
                    //订单创建 成功后就发消息
                    rabbitTemplate.convertAndSend("order-event-exchange", "order.create.order", order.getOrderEntity());
                    return respVo;
                } else {
                    //库存锁定失败
                    respVo.setCode(3);
                    String msg = (String) r.get("msg");
                    throw new NoStockException(msg);
                }
            } else {
                respVo.setCode(2);
                return respVo;
            }
        }
    }

    //amqp关闭订单
    @Override
    public void orderClose(OrderEntity orderEntity) {
        OrderEntity entity = this.getById(orderEntity.getId());
        if (entity.getStatus() == OrderStatusEnum.CREATE_NEW.getCode()) {
            System.out.println("订单已超时开始关闭订单......");
            //如果是未付款就关单 并设置订单状态
            OrderEntity update = new OrderEntity();
            update.setId(orderEntity.getId());
            update.setStatus(OrderStatusEnum.CANCLED.getCode());
            this.updateById(update);
            //发送给mq消息解锁库存
            OrderTo orderTo = new OrderTo();
            BeanUtils.copyProperties(entity, orderTo);
            rabbitTemplate.convertAndSend("order-event-exchange", "order.release.other", orderTo);
        }
    }


    //获取当前订单支付信息
    @Override
    public PayVo getOrderPay(String orderSn) {
        PayVo payVo = new PayVo();
        OrderEntity orderEntity = getOne(new QueryWrapper<OrderEntity>().eq("order_sn", orderSn));
        BigDecimal payAmount = orderEntity.getPayAmount().setScale(2, BigDecimal.ROUND_UP);
        payVo.setTotal_amount(payAmount.toString());
        payVo.setOut_trade_no(orderSn);
        List<OrderItemEntity> entities = orderItemService.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", orderSn));
        payVo.setSubject(entities.get(0).getSkuName());
        payVo.setBody(entities.get(0).getSkuAttrsVals());

        return payVo;
    }

    //查询订单详情
    @Override
    public PageUtils queryPageWithItem(Map<String, Object> params) {

        MemberRespVo memberRespVo = LoginUserInterceptor.threadLocal.get();
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>().eq("member_id", memberRespVo.getId()).orderByDesc("id")
        );
        List<OrderEntity> orderEntities = page.getRecords().stream().map(order -> {
            List<OrderItemEntity> orderItemEntityList = orderItemService.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", order.getOrderSn()));
            order.setOrderItemEntities(orderItemEntityList);
            return order;
        }).collect(Collectors.toList());

        page.setRecords(orderEntities);
        return new PageUtils(page);
    }

    //处理支付宝支付结果
    @Override
    public String handlePayResult(PayAsyncVo vo) {

        // 1、保存交易流水信息
        PaymentInfoEntity paymentInfoEntity = new PaymentInfoEntity();
        paymentInfoEntity.setAlipayTradeNo(vo.getTrade_no());
        paymentInfoEntity.setOrderSn(vo.getOut_trade_no());
        paymentInfoEntity.setPaymentStatus(vo.getTrade_status());
        paymentInfoEntity.setCallbackTime(vo.getNotify_time());
        paymentInfoService.save(paymentInfoEntity);

// 2、修改订单状态信息
        if (vo.getTrade_status().equals("TRADE_SUCCESS") || vo.getTrade_status().equals("TRADE_FINISHED")) {
            // 支付成功
            String orderSn = vo.getOut_trade_no();
            this.baseMapper.updateOrderStatus(orderSn, OrderStatusEnum.PAYED.getCode());
        }
        return "success";
    }

    //创建秒杀单
    @Override
    public void createSeckillOrder(SecKillOrderTo to) {

        // TODO 设置详细信息

        // 保存订单信息
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(to.getOrderSn());
        orderEntity.setMemberId(to.getMemberId());

        orderEntity.setStatus(OrderStatusEnum.CREATE_NEW.getCode());
        BigDecimal totalPrice = to.getSeckillPrice().multiply(new BigDecimal(to.getNum() + ""));
        orderEntity.setPayAmount(totalPrice);

        this.save(orderEntity);

        OrderItemEntity orderItemEntity = new OrderItemEntity();
        orderItemEntity.setOrderSn(to.getOrderSn());
        orderItemEntity.setRealAmount(totalPrice);
        orderItemEntity.setSkuQuantity(to.getNum());

        orderItemService.save(orderItemEntity);

    }

    //保存订单
    private void saveOrder(OrderCreateTo order) {
        OrderEntity orderEntity = order.getOrderEntity();
        orderEntity.setModifyTime(new Date());
        this.save(orderEntity);

        List<OrderItemEntity> orderItems = order.getItems();
        orderItemService.saveBatch(orderItems);
    }

    //下单操作
    private OrderCreateTo createOrder() {

        OrderCreateTo createTo = new OrderCreateTo();
        //1.生成订单

        //唯一订单号
        String orderSn = IdWorker.getTimeId();
        //订单项中的收货等信息
        OrderEntity orderEntity = buildOrder(orderSn);
        createTo.setOrderEntity(orderEntity);

        //2.构建当前用户的订单购物项
        List<OrderItemEntity> orderItemEntities = buildOrderItems(orderSn);
        createTo.setItems(orderItemEntities);

        //3.计算价格相关
        computePrice(orderEntity, orderItemEntities);

        return createTo;

    }

    private void computePrice(OrderEntity orderEntity, List<OrderItemEntity> orderItemEntities) {
        BigDecimal totalPrice = new BigDecimal("0.0");
        BigDecimal totalGiftIntegration = new BigDecimal("0.0");
        BigDecimal totalGiftGrowth = new BigDecimal("0.0");
        //订单总额 各项相加
        for (OrderItemEntity entity : orderItemEntities) {
            BigDecimal realAmount = entity.getRealAmount();
            totalPrice = totalPrice.add(realAmount);

            totalGiftIntegration = totalGiftIntegration.add(new BigDecimal(entity.getGiftIntegration().toString()));
            totalGiftGrowth = totalGiftGrowth.add(new BigDecimal(entity.getGiftGrowth()));
        }
        //订单相关总额
        orderEntity.setTotalAmount(totalPrice);
        orderEntity.setPayAmount(totalPrice.add(orderEntity.getFreightAmount()));
        orderEntity.setIntegration(totalGiftIntegration.intValue());
        orderEntity.setGrowth(totalGiftGrowth.intValue());
    }

    //构建订单项中收货等信息
    private OrderEntity buildOrder(String orderSn) {
        //订单项实体
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(orderSn);
        //设置会员信息
        MemberRespVo memberRespVo = LoginUserInterceptor.threadLocal.get();
        orderEntity.setMemberId(memberRespVo.getId());
        orderEntity.setMemberUsername(memberRespVo.getNickname());
        //获取收货信息
        OrderSubmitVo submitVo = orderSubmitVoThreadLocal.get();
        R r = wareFeignService.fare(submitVo.getAddrId());
        FareVo fareVo = r.getData(new TypeReference<FareVo>() {
        });
        //运费金额 收货人信息
        orderEntity.setFreightAmount(fareVo.getFare());
        orderEntity.setReceiverProvince(fareVo.getAddress().getProvince());
        orderEntity.setReceiverCity(fareVo.getAddress().getCity());
        orderEntity.setReceiverRegion(fareVo.getAddress().getRegion());
        orderEntity.setReceiverDetailAddress(fareVo.getAddress().getDetailAddress());
        orderEntity.setReceiverName(fareVo.getAddress().getName());
        orderEntity.setReceiverPhone(fareVo.getAddress().getPhone());
        orderEntity.setReceiverPostCode(fareVo.getAddress().getPostCode());
        orderEntity.setStatus(OrderStatusEnum.CREATE_NEW.getCode());
        orderEntity.setAutoConfirmDay(7);
        return orderEntity;
    }

    //构建订单中的商品项
    private List<OrderItemEntity> buildOrderItems(String orderSn) {
        List<OrderItemVo> currentUserCartItems = cartFeignService.getCurrentUserCartItems();
        //将购物项信息封装到订单的每个订单购物项
        if (currentUserCartItems != null && currentUserCartItems.size() > 0) {
            List<OrderItemEntity> orderItemEntities = currentUserCartItems.stream().map(cartItem -> {
                OrderItemEntity itemEntity = buildOrderItem(cartItem);
                itemEntity.setOrderSn(orderSn);
                return itemEntity;
            }).collect(Collectors.toList());
            return orderItemEntities;
        }
        return null;
    }

    //构建每个订单的购物项
    private OrderItemEntity buildOrderItem(OrderItemVo cartItem) {
        OrderItemEntity orderItemEntity = new OrderItemEntity();

        // 商品spu信息
        R r = productFeignService.getSpuInfoBySkuId(cartItem.getSkuId());
        SpuInfoVo spuInfoVo = r.getData(new TypeReference<SpuInfoVo>() {
        });
        orderItemEntity.setSpuId(spuInfoVo.getId());
        orderItemEntity.setSpuBrand(spuInfoVo.getBrandId().toString());
        orderItemEntity.setSpuName(spuInfoVo.getSpuName());
        orderItemEntity.setCategoryId(spuInfoVo.getCatalogId());

        // sku信息
        orderItemEntity.setSkuId(cartItem.getSkuId());
        orderItemEntity.setSkuName(cartItem.getTitle());
        orderItemEntity.setSkuPic(cartItem.getImage());
        orderItemEntity.setSkuPrice(cartItem.getPrice());
        String skuAttr = StringUtils.collectionToDelimitedString(cartItem.getSkuAttr(), ";");
        orderItemEntity.setSkuAttrsVals(skuAttr);
        orderItemEntity.setSkuQuantity(cartItem.getCount());

        // 成长值、积分信息
        // TODO 模拟数据
        int giftGrowth = (cartItem.getTotalPrice().intValue()) / 10;
        orderItemEntity.setGiftGrowth(giftGrowth);
        orderItemEntity.setGiftIntegration(giftGrowth);
        // 该商品经过优惠后的分解金额
        // TODO 实现优惠后的价格
        orderItemEntity.setRealAmount(orderItemEntity.getSkuPrice().multiply(new BigDecimal(orderItemEntity.getSkuQuantity().toString())));

        return orderItemEntity;

    }

}