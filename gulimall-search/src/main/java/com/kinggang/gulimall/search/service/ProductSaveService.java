package com.kinggang.gulimall.search.service;

import com.kinggang.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

public interface ProductSaveService {

    /**
     * 上传到es
     * @return true 有错误 false 无错误
     * @throws IOException
     */

    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
