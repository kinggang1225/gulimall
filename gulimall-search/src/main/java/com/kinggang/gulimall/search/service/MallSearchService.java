package com.kinggang.gulimall.search.service;

import com.kinggang.gulimall.search.vo.SearchParam;
import com.kinggang.gulimall.search.vo.SearchResult;

public interface MallSearchService {
    SearchResult search(SearchParam param);
}
