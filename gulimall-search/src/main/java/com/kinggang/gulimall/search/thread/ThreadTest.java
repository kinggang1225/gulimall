package com.kinggang.gulimall.search.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {


    /**
     * 创建线程的四种方法
     * <p>
     * 1.继承Thread类
     * Thread1 thread1 = new Thread1();
     * thread1.start();
     * <p>
     * 2.实现Runnable接口
     * Runnable1 runnable1 = new Runnable1();
     * new Thread(runnable1).start();
     * <p>
     * 3.实现Callable接口  +FutureTask(可以拿到返回结果.可以处理异常)
     * FutureTask<Integer> futureTask = new FutureTask<>(new Callable1());
     * new Thread(futureTask).start();
     * //阻塞的等待线程结束返回结果
     * try {
     * Integer integer = futureTask.get();
     * } catch (InterruptedException e) {
     * e.printStackTrace();
     * } catch (ExecutionException e) {
     * e.printStackTrace();
     * }
     * 4.线程池
     * 给线程池提交任务
     * //给线程池提交线程
     * service.execute(new Runnable1());
     * <p>
     * 区别:1 2 不可以得到返回值 3 可以得到返回值 但都不能资源控制  系统性能稳定
     *
     * @param args
     */

    //new 一个可以装10个线程的线程池
    public static ExecutorService service = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {
        System.out.println("main....start..");
        //给线程池提交线程
        service.execute(new Runnable1());

        System.out.println("main....end..");

    }


    public static class Thread1 extends Thread {
        @Override
        public void run() {

            System.out.println("当前线程号" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果:i = " + i);
        }
    }

    public static class Runnable1 implements Runnable {

        @Override
        public void run() {
            System.out.println("当前线程号" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果:i = " + i);
        }
    }

    public static class Callable1 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程号" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果:i = " + i);
            return i;
        }
    }
}
