package com.kinggang.gulimall.product.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class AttrRespVo extends AttrVo implements Serializable {

    //分类名
    private String catelogName;
    //分组名
    private String groupName;

    //分类全路径
    private Long[] catelogPath;
}
