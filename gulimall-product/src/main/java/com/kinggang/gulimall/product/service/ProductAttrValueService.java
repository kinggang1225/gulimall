package com.kinggang.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:40
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> collect);

    List<ProductAttrValueEntity> baseListforspu(Long spuId);

    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);
}

