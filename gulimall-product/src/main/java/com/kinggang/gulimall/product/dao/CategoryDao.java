package com.kinggang.gulimall.product.dao;

import com.kinggang.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:42
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
