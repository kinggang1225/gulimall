package com.kinggang.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRedissonConfig {

    @Bean(destroyMethod = "shutdown")
    public RedissonClient client() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://123.56.133.0:6379");
       // config.useSingleServer().setPassword("1171233");
        return Redisson.create(config);
    }
}
