package com.kinggang.gulimall.product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kinggang.gulimall.product.entity.AttrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品属性
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:42
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {

    List<Long> getSearchTypeAttrs(@Param("ids") List<Long> ids);
}
