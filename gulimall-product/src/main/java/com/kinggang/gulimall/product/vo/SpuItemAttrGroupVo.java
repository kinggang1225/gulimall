package com.kinggang.gulimall.product.vo;

import lombok.Data;

import java.util.List;

@Data
public  class SpuItemAttrGroupVo {
    private String groupName;
    List<Attr> attrs;
}
