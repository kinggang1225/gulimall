package com.kinggang.gulimall.product.vo;

import lombok.Data;

@Data
public class BrandVo {

    Long brandId;

    String brandName;
}
