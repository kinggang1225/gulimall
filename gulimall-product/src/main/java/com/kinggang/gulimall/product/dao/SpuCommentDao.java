package com.kinggang.gulimall.product.dao;

import com.kinggang.gulimall.product.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:39
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
