package com.kinggang.gulimall.product.feign.fallback;

import com.kinggang.common.exception.BizCodeEnume;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.product.feign.SecKillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SecKillFeignServiceFallBack implements SecKillFeignService {
    @Override
    public R getSkuSeckillInfo(Long skuId) {
        log.error("SecKillFeignService熔断保护");
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(), BizCodeEnume.TO_MANY_REQUEST.getMsg());
    }
}
