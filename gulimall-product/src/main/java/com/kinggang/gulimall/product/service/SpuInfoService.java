package com.kinggang.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.product.entity.SpuInfoDescEntity;
import com.kinggang.gulimall.product.entity.SpuInfoEntity;
import com.kinggang.gulimall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:39
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SpuSaveVo vo);


    PageUtils queryPageByCondition(Map<String, Object> params);

    void spuUp(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);
}

