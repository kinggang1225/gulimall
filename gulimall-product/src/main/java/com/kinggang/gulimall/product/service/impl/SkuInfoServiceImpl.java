package com.kinggang.gulimall.product.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.product.dao.SkuInfoDao;
import com.kinggang.gulimall.product.entity.SkuImagesEntity;
import com.kinggang.gulimall.product.entity.SkuInfoEntity;
import com.kinggang.gulimall.product.entity.SpuInfoDescEntity;
import com.kinggang.gulimall.product.feign.SecKillFeignService;
import com.kinggang.gulimall.product.service.AttrGroupService;
import com.kinggang.gulimall.product.service.SkuImagesService;
import com.kinggang.gulimall.product.service.SkuInfoService;
import com.kinggang.gulimall.product.service.SpuInfoDescService;
import com.kinggang.gulimall.product.vo.SecKillInfoVo;
import com.kinggang.gulimall.product.vo.SkuItemSaleAttrVo;
import com.kinggang.gulimall.product.vo.SkuItemVo;
import com.kinggang.gulimall.product.vo.SpuItemAttrGroupVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Autowired
    SkuImagesService imagesService;

    @Autowired
    SpuInfoDescService spuInfoDescService;

    @Autowired
    AttrGroupService attrGroupService;

    @Autowired
    SkuSaleAttrValueServiceImpl skuSaleAttrValueService;

    @Autowired
    ThreadPoolExecutor executor;

    @Autowired
    SecKillFeignService secKillFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    //通过spuid查询所有的sku信息
    @Override
    public List<SkuInfoEntity> getSkusBySpuId(Long spuId) {
        List<SkuInfoEntity> entityList = this.list(new QueryWrapper<SkuInfoEntity>().eq("spu_id", spuId));
        return entityList;
    }

    @Override
    //异步编排模式
    public SkuItemVo item(Long skuId) throws ExecutionException, InterruptedException {
        SkuItemVo skuItemVo = new SkuItemVo();

        //开始异步操作
        CompletableFuture<SkuInfoEntity> infoFuture = CompletableFuture.supplyAsync(() -> {
            //设置sku的基本信息
            SkuInfoEntity info = getById(skuId);
            skuItemVo.setInfo(info);
            return info;
        }, executor);

        CompletableFuture<Void> saleFuture = infoFuture.thenAcceptAsync(res -> {
            //获取spu销售属性组合
            List<SkuItemSaleAttrVo> saleAttrVo = skuSaleAttrValueService.getSaleAttrsBySpuId(res.getSpuId());
            skuItemVo.setSaleAttr(saleAttrVo);
        }, executor);

        CompletableFuture<Void> descFuture = infoFuture.thenAcceptAsync(res -> {
            //获取spu的图片介绍
            SpuInfoDescEntity infoDescEntity = spuInfoDescService.getById(res.getSpuId());
            skuItemVo.setDesp(infoDescEntity);
        }, executor);

        CompletableFuture<Void> baseFuture = infoFuture.thenAcceptAsync(res -> {
            //获取spu的规格参数介绍
            List<SpuItemAttrGroupVo> attrGroupVos = attrGroupService.getAttrGroupWithAttrsBySpuId(res.getSpuId(), res.getCatalogId());
            skuItemVo.setGroupAttrs(attrGroupVos);
        }, executor);


        CompletableFuture<Void> imageFuture = CompletableFuture.runAsync(() -> {
            //设置sku的图片信息
            List<SkuImagesEntity> imagesEntities = imagesService.getImagesBySkuId(skuId);
            skuItemVo.setImages(imagesEntities);
        }, executor);


        //秒杀信息
        CompletableFuture<Void> seckillFuture = CompletableFuture.runAsync(() -> {
            R r = secKillFeignService.getSkuSeckillInfo(skuId);
            if (r.getCode() == 0) {
                SecKillInfoVo secKillInfoVo = r.getData(new TypeReference<SecKillInfoVo>() {
                });
                skuItemVo.setSecKillInfoVo(secKillInfoVo);
            }
        }, executor);
        //等待以上所有的都执行完就进行编排
        CompletableFuture.allOf(saleFuture, descFuture, baseFuture, imageFuture, seckillFuture).get();


        return skuItemVo;
    }

}