package com.kinggang.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.product.entity.AttrEntity;
import com.kinggang.gulimall.product.entity.ProductAttrValueEntity;
import com.kinggang.gulimall.product.vo.AttrRespVo;
import com.kinggang.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:42
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryBasePage(Map<String, Object> params, Long catelogId, String type);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    PageUtils getNoRelationAttr(Long attrgroupId, Map<String, Object> params);

    List<Long> getSearchTypeAttrs(List<Long> ids);
}

