package com.kinggang.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.common.utils.Query;
import com.kinggang.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.kinggang.gulimall.product.dao.AttrGroupDao;
import com.kinggang.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.kinggang.gulimall.product.entity.AttrEntity;
import com.kinggang.gulimall.product.entity.AttrGroupEntity;
import com.kinggang.gulimall.product.service.AttrGroupService;
import com.kinggang.gulimall.product.service.AttrService;
import com.kinggang.gulimall.product.vo.AttrGroupRelationVo;
import com.kinggang.gulimall.product.vo.AttrGroupWithAttrsVo;
import com.kinggang.gulimall.product.vo.SkuItemVo;
import com.kinggang.gulimall.product.vo.SpuItemAttrGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    AttrAttrgroupRelationDao relationDao;

    @Autowired
    AttrService attrService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        //查询条件
        QueryWrapper<AttrGroupEntity> wrapper = new QueryWrapper<AttrGroupEntity>();
        //传过来的按关键字查询参数
        String key = (String) params.get("key");
        //如果有按关键字查询
        if (!StringUtils.isEmpty(key)) {
            wrapper.and(obj -> {
                obj.eq("attr_group_id", key).or().like("attr_group_name", key);
            });
        }
        if (catelogId == 0) {
            //没有分类id
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), wrapper);
            return new PageUtils(page);
        } else {
            wrapper.eq("catelog_id", catelogId);
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), wrapper);
            return new PageUtils(page);
        }
    }

    //删除属性分组关系
    @Override
    public void deleteRelation(AttrGroupRelationVo[] vos) {
        //relationService.  remove(new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", ).eq("attr_group_id", ))
        //批量删除关系
        List<AttrAttrgroupRelationEntity> collect = Arrays.asList(vos).stream().map((item) -> {
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(item, relationEntity);
            return relationEntity;
        }).collect(Collectors.toList());
        relationDao.deleteBatchRelation(collect);
    }

    //根据分类查询属性及属性分类
    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrsByCatelogId(Long catelogId) {
        //查出分组
        List<AttrGroupEntity> groupEntity = this.list(new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId));
        List<AttrGroupWithAttrsVo> collect = groupEntity.stream().map(item -> {
            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(item, attrsVo);
            //根据分组id 获得属性集合
            List<AttrEntity> attrEntities = attrService.getRelationAttr(item.getAttrGroupId());
            attrsVo.setAttrs(attrEntities);
            return attrsVo;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId) {
       return baseMapper.getAttrGroupWithAttrsBySpuId(spuId,catalogId);
    }

}