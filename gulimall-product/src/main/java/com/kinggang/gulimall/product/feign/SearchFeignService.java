package com.kinggang.gulimall.product.feign;

import com.kinggang.common.to.es.SkuEsModel;
import com.kinggang.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("gulimall-search")
@Component
public interface SearchFeignService {

    @PostMapping("/search/save/product")
    R procuctStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
