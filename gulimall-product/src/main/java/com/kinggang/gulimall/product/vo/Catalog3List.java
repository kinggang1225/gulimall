package com.kinggang.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
/**
 * 三级分类VO
 */
public class Catalog3List implements Serializable {

    private String catalog2Id;
    private String id;
    private String name;
}