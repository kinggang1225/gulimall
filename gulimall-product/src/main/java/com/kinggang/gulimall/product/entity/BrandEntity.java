package com.kinggang.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.kinggang.common.vaild.AddGroup;
import com.kinggang.common.vaild.ListValue;
import com.kinggang.common.vaild.UpdateGroup;
import com.kinggang.common.vaild.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:41
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id
     */
    @Null(message = "添加时id需要为空", groups = {AddGroup.class})
    @NotNull(message = "修改时id不能为空", groups = {UpdateGroup.class})
    @TableId
    private Long brandId;
    /**
     * 品牌名
     */
    @NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 品牌logo地址
     */
    @URL(message = "logo 地址必须要是合法的URL地址")
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     */
//    @Pattern()
    //自定义校验规则注解
    @NotNull(groups = {AddGroup.class, UpdateStatusGroup.class})
    @ListValue(value = {0, 1}, message = "必须指定商品状态(0 关闭 或 1 开启 )",groups = {AddGroup.class, UpdateStatusGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z]$", message = "首字母必须要是a-z A-Z",groups = {AddGroup.class,UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @NotNull
    @Min(value = 0, message = "必须大于等于0")
    private Integer sort;

}
