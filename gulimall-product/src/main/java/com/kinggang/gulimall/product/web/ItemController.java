package com.kinggang.gulimall.product.web;

import com.kinggang.gulimall.product.feign.CartFeignService;
import com.kinggang.gulimall.product.service.SkuInfoService;
import com.kinggang.gulimall.product.vo.SkuItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.ExecutionException;

@Controller
public class ItemController {

    @Autowired
    SkuInfoService skuInfoService;

    @Autowired
    CartFeignService cartFeignService;

    @GetMapping("/{skuId}.html")
    public String item(@PathVariable(value = "skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {
        SkuItemVo skuItemVo = skuInfoService.item(skuId);
        model.addAttribute("item", skuItemVo);
        return "item";
    }

    @GetMapping("/haha")
    @ResponseBody
    public String hello() {
        return cartFeignService.hello();
    }
}
