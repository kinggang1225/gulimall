package com.kinggang.gulimall.product.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient("gulimall-cart")
@Component
public interface CartFeignService {

    @GetMapping("/hello")
    @ResponseBody
    String hello();
}
