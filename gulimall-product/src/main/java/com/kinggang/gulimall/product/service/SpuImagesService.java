package com.kinggang.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:39
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveImages(Long id, List<String> images);
}

