package com.kinggang.gulimall.product.vo;

import com.kinggang.gulimall.product.entity.SkuImagesEntity;
import com.kinggang.gulimall.product.entity.SkuInfoEntity;
import com.kinggang.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;


@Data
public class SkuItemVo {

    SkuInfoEntity info; // sku基本信息

    boolean hasStock = true; // 是否有库存

    List<SkuImagesEntity> images; // sku图片信息

    List<SkuItemSaleAttrVo> saleAttr;  // spu的销售属性组合信息

    SpuInfoDescEntity desp; // spu介绍信息

    List<SpuItemAttrGroupVo> groupAttrs; // spu规格参数

    SecKillInfoVo secKillInfoVo; //秒杀信息
}