package com.kinggang.gulimall.product.feign;

import com.kinggang.common.utils.R;
import com.kinggang.gulimall.product.feign.fallback.SecKillFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "gulimall-seckill", fallback = SecKillFeignServiceFallBack.class)
@Component(value = "secKillFeignService")
public interface SecKillFeignService {

    @GetMapping("/sku/skuseckill/{skuId}")
    R getSkuSeckillInfo(@PathVariable("skuId") Long skuId);
}
