package com.kinggang.gulimall.product.dao;

import com.kinggang.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 09:33:41
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
