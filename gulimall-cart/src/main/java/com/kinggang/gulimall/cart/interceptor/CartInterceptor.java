package com.kinggang.gulimall.cart.interceptor;

import com.kinggang.common.constant.CartConstant;
import com.kinggang.common.vo.MemberRespVo;
import com.kinggang.gulimall.cart.vo.UserInfoTo;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * 此拦截器用来判断用户是否登录
 */
public class CartInterceptor implements HandlerInterceptor {

    public static ThreadLocal<UserInfoTo> threadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession();
        MemberRespVo user = (MemberRespVo) session.getAttribute("loginUser");

        UserInfoTo userInfo = new UserInfoTo();
        if (user != null) {
            // 用户登录了
            userInfo.setUserId(user.getId());
        }
        //  判断当前浏览器中的cookie中是否存在临时用户的标识的cookie
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                String name = cookie.getName();
                if (name.equals(CartConstant.TEMP_USER_COOKIE_NAME)) {
                    //是临时用户
                    userInfo.setUserKey(cookie.getValue());
                    userInfo.setTempUser(true); // 设置辨别是否有临时用户的boolean字段
                }
            }
        }
        //给临时用户创建一个keyValue
        if (StringUtils.isEmpty(userInfo.getUserKey())) {
            String uuid = UUID.randomUUID().toString();
            userInfo.setUserKey(uuid);
        }
        //目标方法执行前 放入线程里面共享的数据
        threadLocal.set(userInfo);
        return true;
    }

    //方法执行之后将cooki存入浏览器
    //临时用户保存cookie
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        UserInfoTo userInfoTo = threadLocal.get();
        //如果没有临时用户
        if (!userInfoTo.getTempUser()) {
            Cookie cookie = new Cookie(CartConstant.TEMP_USER_COOKIE_NAME, userInfoTo.getUserKey());
            cookie.setPath("/");
            cookie.setDomain("gulimall.com");
            cookie.setMaxAge(CartConstant.TEMP_USER_COOKIE_TIMEOUT);//设置cookie过期时间为一个月
            response.addCookie(cookie);
        }
    }
}
