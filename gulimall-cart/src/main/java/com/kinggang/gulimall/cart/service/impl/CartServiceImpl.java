package com.kinggang.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.kinggang.common.utils.R;
import com.kinggang.gulimall.cart.feign.ProductFeignService;
import com.kinggang.gulimall.cart.interceptor.CartInterceptor;
import com.kinggang.gulimall.cart.service.CartService;
import com.kinggang.gulimall.cart.vo.Cart;
import com.kinggang.gulimall.cart.vo.CartItem;
import com.kinggang.gulimall.cart.vo.SkuInfoVo;
import com.kinggang.gulimall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    ThreadPoolExecutor executor;

    private final String CART_PREFIX = "gulimall:cart:";

    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        //操作对象
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        String res = (String) ops.get(skuId.toString());
        if (StringUtils.isEmpty(res)) {
            //没这个商品 那么就新增商品
            CartItem cartItem = new CartItem();
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                //远程调用先查询一遍商品信息
                R r = productFeignService.info(skuId);
                if (r.getCode() == 0) {
                    SkuInfoVo skuInfo = r.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                    });
                    cartItem.setCheck(true);
                    cartItem.setCount(num);
                    cartItem.setImage(skuInfo.getSkuDefaultImg());
                    cartItem.setPrice(skuInfo.getPrice());
                    cartItem.setSkuId(skuId);
                    cartItem.setTitle(skuInfo.getSkuTitle());
                }
            }, executor);
            //远程查询sku 组合信息
            CompletableFuture<Void> getSkuSaleAttrValue = CompletableFuture.runAsync(() -> {
                List<String> skuSaleAttrValue = productFeignService.getSkuSaleAttrValue(skuId);
                cartItem.setSkuAttr(skuSaleAttrValue);
            }, executor);

            CompletableFuture.allOf(getSkuInfoTask, getSkuSaleAttrValue).get();

            //存入redis
            String s = JSON.toJSONString(cartItem);
            ops.put(skuId.toString(), s);
            return cartItem;
        } else {
            //以前有商品那么就更新商品数量
            CartItem item = JSON.parseObject(res, CartItem.class);
            item.setCount(item.getCount() + num);
            ops.put(skuId.toString(), JSON.toJSON(item));
            return item;
        }
    }

    @Override
    public Cart getCart() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        Cart cart = new Cart();
        if (userInfoTo.getUserId() != null) {
            //已经登录的用户
            String carKey = CART_PREFIX + userInfoTo.getUserId().toString();

            //还要合并临时购物车的数据 先判断临时购物车有没有数据
            String tempCartKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> tempCartItems = getCartItems(tempCartKey);
            if (tempCartItems != null && tempCartItems.size() > 0) {
                //临时购物车有数据 合并购物车
                tempCartItems.forEach(item -> {
                    try {
                        addToCart(item.getSkuId(), item.getCount());
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
                //清空临时购物车
                clearCart(tempCartKey);
            }
            //获取登录购物车 包含合并后的临时购物车
            List<CartItem> cartItems = getCartItems(carKey);
            cart.setItems(cartItems);
        } else {
            //还未登录
            String carKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> collect = getCartItems(carKey);
            cart.setItems(collect);
        }
        return cart;
    }

    //清空购物车
    @Override
    public void clearCart(String cartKey) {
        redisTemplate.delete(cartKey);
    }

    //选择购物车
    @Override
    public void checkItem(Long skuId, Integer check) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(check == 1 ? true : false);
        ops.put(skuId.toString(), JSON.toJSONString(cartItem));
    }

    //改变商品数量
    @Override
    public void changeItemCount(Long skuId, Integer num) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCount(num);
        ops.put(skuId.toString(), JSON.toJSONString(cartItem));
    }

    //删除商品
    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        ops.delete(skuId.toString());
    }

    //获取当前用户结算的购物项
    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() == null) {
            return null;
        } else {
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);
            //获取所有被选中的购物项
            List<CartItem> collect = cartItems.stream().filter(item -> item.getCheck()).map(item -> {
                //再获取到当前商品的真实价格
                R r = productFeignService.getPrice(item.getSkuId());
                String data = (String) r.get("data");
                item.setPrice(new BigDecimal(data));
                return item;
            }).collect(Collectors.toList());
            return collect;
        }
    }

    //根据skuId 获取购物项
    private CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> ops = getCartOps();
        String item = (String) ops.get(skuId.toString());
        CartItem cartItem = JSON.parseObject(item, CartItem.class);
        return cartItem;
    }

    //根据cartKey获得购物车的所有商品项
    private List<CartItem> getCartItems(String carKey) {
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(carKey);
        List<Object> values = hashOps.values();
        if (values != null && values.size() > 0) {
            List<CartItem> collect = values.stream().map(item -> {

                String str = (String) item;
                CartItem cartItem = JSON.parseObject(str, CartItem.class);

                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    //设置操作购物车的哈希操作对象
    private BoundHashOperations<String, Object, Object> getCartOps() {
        String cartKey = "";
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null) {
            //已经登录的用户
            cartKey = CART_PREFIX + userInfoTo.getUserId();
        } else {
            cartKey = CART_PREFIX + userInfoTo.getUserKey();
        }

        BoundHashOperations<String, Object, Object> hashOperations = redisTemplate.boundHashOps(cartKey);
        return hashOperations;
    }
}
