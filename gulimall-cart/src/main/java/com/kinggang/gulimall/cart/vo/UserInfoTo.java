package com.kinggang.gulimall.cart.vo;


import lombok.Data;

@Data
public class UserInfoTo {

    private Long userId;
    private String userKey;

    //是否有临时用户了
    private Boolean tempUser = false;
}
