package com.kinggang.gulimall.seckill.scheduled;


import com.kinggang.gulimall.seckill.service.SeckillService;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 每天晚上三点上架最近三天需要上架的商品
 */
@Service
public class SeckillSkuScheduled {

    @Autowired
    SeckillService seckillService;

    @Autowired
    RedissonClient redissonClient;

    private final String upload_lock = "seckill:upload:lock";


    //每天晚上三点
    @Scheduled(cron = "0 * * * * ?")
    public void uploadSeckillSkuLatest3Days() {
        //加入分布式锁 锁的业务完成状态也就更新完成 释放锁后,其他人拿到锁后才能获得最新状态
        RLock lock = redissonClient.getLock(upload_lock);
        lock.lock(10, TimeUnit.SECONDS);
        System.out.println("开始任务.....................");
        try {
            seckillService.uploadSeckillSkuLatest3Days();
        } finally {
            lock.unlock();

        }
    }
}
