package com.kinggang.gulimall.seckill.interceptor;

import com.kinggang.common.constant.AuthServerConstant;
import com.kinggang.common.vo.MemberRespVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginUserInterceptor implements HandlerInterceptor {
    //系统共享登录用户信息
    public static ThreadLocal<MemberRespVo> threadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        // 判断本次请求是不是服务间的远程调用，如果是则不需要进行登录判断 - 库存服务调用订单服务获取订单信息
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        boolean match = antPathMatcher.match("/kill", requestURI);

        if (match) {
            MemberRespVo loginUser = (MemberRespVo) request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
            if (loginUser != null) {
                //已经登录
                threadLocal.set(loginUser);
                return true;
            } else {
                //去登陆
                request.getSession().setAttribute("msg", "你还没有登录请先登录后再操作...");
                response.sendRedirect("http://auth.gulimall.com/login.html");
                return true;
            }
        }
        return true;
    }
}
