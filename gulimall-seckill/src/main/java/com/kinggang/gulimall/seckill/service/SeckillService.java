package com.kinggang.gulimall.seckill.service;


import com.kinggang.gulimall.seckill.to.SecKillSkuRedisTo;

import java.util.List;

public interface SeckillService {

    void uploadSeckillSkuLatest3Days();

    List<SecKillSkuRedisTo> getCurrentSeckillSkus();

    SecKillSkuRedisTo getSkuSeckillInfo(Long skuId);

    String secKill(String killId, String key, Integer num);
}
