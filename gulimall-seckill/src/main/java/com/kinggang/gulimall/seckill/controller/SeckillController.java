package com.kinggang.gulimall.seckill.controller;


import com.kinggang.common.utils.R;
import com.kinggang.gulimall.seckill.service.SeckillService;
import com.kinggang.gulimall.seckill.to.SecKillSkuRedisTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class SeckillController {

    @Autowired
    SeckillService seckillService;


    @ResponseBody
    @GetMapping("/getCurrentSeckillSkus")
    public R getCurrentSeckillSkus() {
        List<SecKillSkuRedisTo> skus = seckillService.getCurrentSeckillSkus();
        return R.ok().setData(skus);
    }

    @ResponseBody
    @GetMapping("/sku/skuseckill/{skuId}")
    public R getSkuSeckillInfo(@PathVariable("skuId") Long skuId) {
        SecKillSkuRedisTo skus = seckillService.getSkuSeckillInfo(skuId);
        return R.ok().setData(skus);
    }

    //http://seckill.gulimall.com/kill?killId=1_35&key=f8fefecdab03463fa56591baf52d83f9&num=1
    @GetMapping("/kill")
    public String secKIll(Model model, @RequestParam("killId") String killId, @RequestParam("key") String key, @RequestParam("num") Integer num) {
        //先要拦截器检查是否登录
        String orderSn = seckillService.secKill(killId, key, num);
        model.addAttribute("orderSn", orderSn);
        return "success";
    }

}
