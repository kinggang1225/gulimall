package com.kinggang.gulimall.seckill.service.imp;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.kinggang.common.to.mq.SecKillOrderTo;
import com.kinggang.common.utils.R;
import com.kinggang.common.vo.MemberRespVo;
import com.kinggang.gulimall.seckill.feign.CouponFeignService;
import com.kinggang.gulimall.seckill.feign.ProductFeignService;
import com.kinggang.gulimall.seckill.interceptor.LoginUserInterceptor;
import com.kinggang.gulimall.seckill.service.SeckillService;
import com.kinggang.gulimall.seckill.to.SecKillSkuRedisTo;
import com.kinggang.gulimall.seckill.vo.SeckillSessionsWithSkus;
import com.kinggang.gulimall.seckill.vo.SkuInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Service
@Slf4j
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    CouponFeignService couponFeignService;

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    RabbitTemplate rabbitTemplate;

    private final String SESSIONS_CACHE_PREFIX = "seckill:sessions:";
    private final String SKUSKILL_CACHE_PREFIX = "seckill:skus";
    private final String SKU_STOCK_SEMAPHORE = "seckill:stock:";

    @Override
    public void uploadSeckillSkuLatest3Days() {
        //扫描最近三天需要上架的商品
        R r = couponFeignService.getLatest3DaysSession();
        if (r.getCode() == 0) {
            List<SeckillSessionsWithSkus> sessionsData = r.getData(new TypeReference<List<SeckillSessionsWithSkus>>() {
            });
            //装入到redis中
            //1.缓存活动信息
            saveSessionInfos(sessionsData);
            //2.缓存活动关联的商品信息
            saveSessionSkuInfos(sessionsData);
        }
    }


    //获取当前时间段要秒杀商品信息
    @Override
    public List<SecKillSkuRedisTo> getCurrentSeckillSkus() {
        //当前时间
        long time = new Date().getTime();
        /**
         * 自定义异常抛出服务限流
         */
        try (Entry entry = SphU.entry("seckillSkus")) {

            Set<String> keys = redisTemplate.keys(SESSIONS_CACHE_PREFIX + "*");
            //sessions:1596772531000-1596801351000
            for (String key : keys) {
                String replace = key.replace(SESSIONS_CACHE_PREFIX, "");
                String[] split = replace.split("-");
                long start = Long.parseLong(split[0]);
                long end = Long.parseLong(split[1]);
                if (time >= start && time <= end) {
                    //获取当前场次的商品信息
                    List<String> sessions = redisTemplate.opsForList().range(key, -100, 100);
                    BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SKUSKILL_CACHE_PREFIX);
                    List<String> list = ops.multiGet(sessions);
                    if (list != null) {
                        List<SecKillSkuRedisTo> redisTos = list.stream().map(item -> {
                            SecKillSkuRedisTo redisTo = JSON.parseObject(item, SecKillSkuRedisTo.class);
                            return redisTo;
                        }).collect(Collectors.toList());
                        return redisTos;
                    }
                    break;
                }
            }
        } catch (BlockException e) {
            log.error("资源被限流{}", e.getMessage());
        }
        return null;
    }

    @Override
    public SecKillSkuRedisTo getSkuSeckillInfo(Long skuId) {
        BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SKUSKILL_CACHE_PREFIX);
        Set<String> keys = ops.keys();
        if (keys != null && keys.size() > 0) {
            //正则匹配 1_2 的对应关系
            String regx = "\\d_" + skuId;
            for (String key : keys) {
                if (Pattern.matches(regx, key)) {
                    String s = ops.get(key);
                    SecKillSkuRedisTo redisTo = JSON.parseObject(s, SecKillSkuRedisTo.class);
                    //判断当前是否是秒杀时间
                    Long startTime = redisTo.getStartTime();
                    Long endTime = redisTo.getEndTime();
                    long currentTime = new Date().getTime();
                    if (currentTime >= startTime && currentTime <= endTime) {
                        //在秒杀时间范围内 可以返回随机码
                    } else {
                        //不是秒杀时间范围 做空随机码
                        redisTo.setRandomCode(null);
                    }
                    return redisTo;
                }
            }
        }
        return null;
    }

    @Override
    public String secKill(String killId, String key, Integer num) {
        MemberRespVo memberRespVo = LoginUserInterceptor.threadLocal.get();
        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SKUSKILL_CACHE_PREFIX);
        String s = hashOps.get(killId);
        if (!StringUtils.isEmpty(s)) {
            SecKillSkuRedisTo redisTo = JSON.parseObject(s, SecKillSkuRedisTo.class);
            //验证 数据合法性
            Long startTime = redisTo.getStartTime();
            Long endTime = redisTo.getEndTime();
            long time = new Date().getTime();
            if (time >= startTime && time <= endTime) {
                String kill = redisTo.getPromotionSessionId() + "_" + redisTo.getSkuId();
                if (key.equals(redisTo.getRandomCode()) && killId.equals(kill)) {
                    //验证随机码和skillId通过
                    //验证购买数量
                    if (num <= redisTo.getSeckillLimit()) {
                        //验证是否已经购买过 使用userId_sessionId_skuId作为标识 购买数量作为值占位
                        String redisKey = memberRespVo.getId() + "_" + kill;
                        //设置占位过期时间为endTime-time
                        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(redisKey, num.toString(), endTime - time, TimeUnit.MILLISECONDS);
                        if (aBoolean) {
                            //占位成功 第一次购买 可以购买
                            //获取信号量减库存
                            RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + redisTo.getRandomCode());
                            //减库存
                            semaphore.tryAcquire(num);
                            //秒杀成功 快速下单 发送MQ消息
                            String orderSn = IdWorker.getTimeId();
                            SecKillOrderTo orderTo = new SecKillOrderTo();
                            orderTo.setOrderSn(orderSn);
                            orderTo.setSkuId(redisTo.getSkuId());
                            orderTo.setMemberId(memberRespVo.getId());
                            orderTo.setNum(num);
                            orderTo.setPromotionSessionId(redisTo.getPromotionSessionId());
                            orderTo.setSeckillPrice(redisTo.getSeckillPrice());

                            rabbitTemplate.convertAndSend("order-event-exchange", "order.seckill.order", orderTo);
                            //返回订单号
                            return orderSn;
                        }
                        return null;
                    }
                    return null;
                }
                return null;
            }
            return null;
        }
        return null;
    }

    //保存秒杀场次信息
    private void saveSessionInfos(List<SeckillSessionsWithSkus> sessions) {
        if (sessions != null) {
            sessions.stream().forEach(item -> {
                long startTime = item.getStartTime().getTime();
                long endTime = item.getEndTime().getTime();
                String key = SESSIONS_CACHE_PREFIX + startTime + "-" + endTime;
                Boolean hasKey = redisTemplate.hasKey(key);
                if (!hasKey) {
                    //收集当前场次的商品信息
                    List<String> skuIds = item.getRelationEntities().stream().map(sku -> sku.getPromotionSessionId() + "_" + sku.getSkuId().toString()).collect(Collectors.toList());
                    //保存到redis
                    redisTemplate.opsForList().leftPushAll(key, skuIds);
                }
            });
        }
    }


    //保存秒杀Sku 信息
    private void saveSessionSkuInfos(List<SeckillSessionsWithSkus> sessions) {
        if (sessions != null) {
            //准备哈希操作
            BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(SKUSKILL_CACHE_PREFIX);
            sessions.stream().forEach(item -> {
                item.getRelationEntities().stream().forEach(sku -> {
                    //设置随机码 防止秒杀请求乱发 作为一个信号量
                    String token = UUID.randomUUID().toString().replace("-", "");

                    if (!ops.hasKey(sku.getPromotionSessionId().toString() + "_" + sku.getSkuId().toString())) {
                        //缓存商品
                        SecKillSkuRedisTo redisTo = new SecKillSkuRedisTo();
                        //sku基本信息
                        R r = productFeignService.getSkuInfo(sku.getSkuId());
                        if (r.getCode() == 0) {
                            SkuInfoVo skuInfo = r.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                            });
                            redisTo.setSkuInfoVo(skuInfo);
                        }
                        //sku秒杀信息
                        BeanUtils.copyProperties(sku, redisTo);
                        //设置活动时间
                        redisTo.setStartTime(item.getStartTime().getTime());
                        redisTo.setEndTime(item.getEndTime().getTime());

                        redisTo.setRandomCode(token);
                        //引入分布式信号量 将商品的秒杀数量作为一个信号量 限流
                        RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + token);
                        semaphore.trySetPermits(sku.getSeckillCount());

                        String s = JSON.toJSONString(redisTo);
                        ops.put(sku.getPromotionSessionId().toString() + "_" + sku.getSkuId().toString(), s);

                    }

                });
            });
        }
    }




}
