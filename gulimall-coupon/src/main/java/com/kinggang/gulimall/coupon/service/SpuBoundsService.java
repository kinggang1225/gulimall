package com.kinggang.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:15
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

