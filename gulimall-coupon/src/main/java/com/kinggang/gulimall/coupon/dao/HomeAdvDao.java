package com.kinggang.gulimall.coupon.dao;

import com.kinggang.gulimall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:17
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
