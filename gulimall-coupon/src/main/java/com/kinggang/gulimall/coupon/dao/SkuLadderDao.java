package com.kinggang.gulimall.coupon.dao;

import com.kinggang.gulimall.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:14
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
