package com.kinggang.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.to.SkuReductionTo;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:14
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo skuReductionTo);
}

