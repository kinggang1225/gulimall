package com.kinggang.gulimall.coupon.dao;

import com.kinggang.gulimall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:15
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
