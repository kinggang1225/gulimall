package com.kinggang.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.coupon.entity.MemberPriceEntity;

import java.util.Map;

/**
 * 商品会员价格
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:15
 */
public interface MemberPriceService extends IService<MemberPriceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

