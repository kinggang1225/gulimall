package com.kinggang.gulimall.coupon.dao;

import com.kinggang.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:18
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
