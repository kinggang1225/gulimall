package com.kinggang.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kinggang.common.utils.PageUtils;
import com.kinggang.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author kinggang
 * @email kinggang@gmail.com
 * @date 2020-06-18 12:41:14
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

